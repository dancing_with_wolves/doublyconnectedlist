run: build
	./test.e
build: List.o test.o
	clang++ List.o test.o -o test.e
main.o: test.cpp
	clang++ -c test.cpp -o test.o
List.o: List.cpp
	clang++ -c List.cpp -o List.o
clean:
	rm *.o
	rm *.e
	rm *.log
