#include <stdio.h>
#include <assert.h>
#include "List.h"

void testListInitAndListDestruct(){

	listInit( nullptr );
	listDestruct( nullptr );

	List_t list = {};
	listInit( &list );
	assert( listOK( &list ) == OK );
	listDestruct( &list );

	List_t list1 = {};
	List_t list2 = {};

	listInit( &list1 );
	listInit( &list2 );

	assert( listOK( &list1 ) == OK );
	assert( listOK( &list2 ) == OK );

	listDestruct( &list1 );
	listDestruct( &list2 );

	printf( "Functions listInit and listDestrut work correct!\n");

}

void testInsertAfter(){
	List_t list = {};
	listInit( &list );

	insertBegin( &list, 1 );
	LIST_DUMP( &list, "testing insertAfter!" );
	assert( listOK( &list ) == OK );

	insertAfter( &list, -1, 1 );
	LIST_DUMP( &list, "testing insertAfter!" );
	assert( listOK( &list ) == OK );

	insertAfter( &list, 0, 2 );
	LIST_DUMP( &list, "testing insertAfter!" );
	assert( listOK( &list ) == OK );

	insertAfter( &list, 1, 3 );
	LIST_DUMP( &list, "testing insertAfter!" );
	assert( listOK( &list ) == OK );

	insertAfter( &list, 0, 4 );
	LIST_DUMP( &list, "testing insertAfter!" );
	assert( listOK( &list ) == OK );

	insertAfter( &list, 0, 5 );
	LIST_DUMP( &list, "testing insertAfter!" );
	assert( listOK( &list ) == OK );

	listDestruct( &list );

	printf( "Function insertAfter works correct!\n");
}

void testInsertBefore(){
	List_t list = {};
	listInit( &list );

	insertBefore( &list, 2, 2 );
	LIST_DUMP( &list, "testing insertBefore!" );
	assert( listOK( &list ) == OK );

	insertBegin( &list, 3 );
	LIST_DUMP( &list, "testing insertBefore!" );
	assert( listOK( &list ) == OK );

	insertBefore( &list, 2, 6 );
	LIST_DUMP( &list, "testing insertBefore!" );
	assert( listOK( &list ) == OK );

	insertBefore( &list, 0, 4 );
	LIST_DUMP( &list, "testing insertBefore!" );
	assert( listOK( &list ) == OK );

	insertBefore( &list, 0, 5 );
	LIST_DUMP( &list, "testing insertBefore!" );
	assert( listOK( &list ) == OK );

	listDestruct( &list );

	printf( "Function insertBefore works correct!\n");
}

void testDelElem(){
	List_t list = {};
	listInit( &list );

	delElem( &list, -15 );
	delElem( &list, 0 );
	delElem( &list, 1 );
	LIST_DUMP( &list, "testing delElem!" );
	assert( listOK( &list ) == OK );

	insertBegin( &list, 1 );
	delElem( &list, 1 );
	LIST_DUMP( &list, "testing delElem!" );
	assert( listOK( &list ) == OK );
	delElem( &list, 0 );
	LIST_DUMP( &list, "testing delElem!" );
	assert( listOK( &list ) == OK );

	insertAfter( &list, 0, 1 );
	insertAfter( &list, 0, 2 );
	LIST_DUMP( &list, "testing delElem!" );
	assert( listOK( &list ) == OK );

	delElem( &list, 1 );
	LIST_DUMP( &list, "testing delElem!" );
	assert( listOK( &list ) == OK );

	delElem( &list, 0 );
	LIST_DUMP( &list, "testing delElem!" );
	assert( listOK( &list ) == OK );

	listDestruct( &list );

	printf( "Deleting elem works correct!\n");
}

void testGetValueByLogicIndexVerySlowlyWorkingDontUseMe(){
	List_t list = {};
	listInit( &list );

	printf( "[%d] value is %d\n", 123, getValueByLogicIndexVerySlowlyWorkingDontUseMe( &list, 123 ) );
	printf( "[%d] value is %d\n", -1, getValueByLogicIndexVerySlowlyWorkingDontUseMe( &list, -1 ) );

	insertBegin( &list, 1 );
	insertAfter( &list, 0, 2 );
	insertAfter( &list, 1, 3 );
	insertAfter( &list, 2, 4 );

	printf( "[%d] value is %d\n", 123, getValueByLogicIndexVerySlowlyWorkingDontUseMe( &list, 123 ) );
	printf( "[%d] value is %d\n", 1, getValueByLogicIndexVerySlowlyWorkingDontUseMe( &list, 1 ) );
	printf( "[%d] value is %d\n", 2, getValueByLogicIndexVerySlowlyWorkingDontUseMe( &list, 2 ) );
	LIST_DUMP( &list, "Looking from testGetValueByLogicIndexVerySlowlyWorkingDontUseMe");

	listDestruct( &list );

	printf( "Getting a value by logic index works correct!\n");
}

void testGetLogicalIndexVerySlowlyWorkingDontUseMe(){
	List_t list = {};
	listInit( &list );


	printf( "%d's logical index is: %d\n", 123., getLogicalIndexVerySlowlyWorkingDontUseMe( &list, 123 ) );
	printf( "%d's logical index is: %d\n", 123., getLogicalIndexVerySlowlyWorkingDontUseMe( &list, 123 ) );

	insertBegin( &list, 1 );
	insertAfter( &list, 0, 2 );
	insertAfter( &list, 1, 3 );
	insertAfter( &list, 2, 4 );

	printf( "%d's logical index is: %d\n", 1., getLogicalIndexVerySlowlyWorkingDontUseMe( &list, 1 ) );
	printf( "%d's logical index is: %d\n", 3., getLogicalIndexVerySlowlyWorkingDontUseMe( &list, 3 ) );
	printf( "%d's logical index is: %d\n", 123., getLogicalIndexVerySlowlyWorkingDontUseMe( &list, 123 ) );
	LIST_DUMP( &list, "Looking from testGetValueByLogicIndexVerySlowlyWorkingDontUseMe");

	insertBefore( &list, 0, 14 );
	insertAfter( &list, 2, 14 );
	insertAfter( &list, 3, 14 );
	insertAfter( &list, 1, 14 );
	insertAfter( &list, 6, 14 );

	LIST_DUMP( &list, "Looking from testGetValueByLogicIndexVerySlowlyWorkingDontUseMe");

	int ans[5] = {};
	getLogicalIndexVerySlowlyWorkingDontUseMe( &list, 14, ans, 5 );
	for( int i = 0; i < 5; i++ ){
		printf( "ans[%d] = %d;\n", i, ans[i] );
	}
	listDestruct( &list );

	printf( "Logical index definition works correct!\n");
}

void testGetElem(){

}

void testLogic(){
	List_t list = {};
	listInit( &list );

	insertBegin( &list, 1 );
	insertAfter( &list, 0, 2 );
	insertAfter( &list, 1, 3 );
	insertAfter( &list, 2, 4 );
	delElem( &list, 1 );
	delElem( &list, 1 );

	insertAfter( &list, 0, 5 );
	insertAfter( &list, 1, 6 );
	LIST_DUMP( &list, "Just looking" );

	listDestruct( &list );
	printf( "Logic works correct!\n");
}

void testSortList(){
	List_t list = {};
	listInit( &list );

	insertBegin( &list, 1 );
	insertAfter( &list, 0, 2 );
	insertBefore( &list, 1, 3 );
	insertAfter( &list, 0, 4 );
	LIST_DUMP( &list, "testing sortList!" );
	printListSlowly( &list );

	sortList( &list );
	LIST_DUMP( &list, "testing sortList!" );
	printListSlowly( &list );
	printSortedList( &list );

	listDestruct( &list );
}

int main(){
	clearLog();

	testDelElem();
	testGetElem();
	testInsertAfter();
	testInsertBefore();
	testListInitAndListDestruct();
	testGetValueByLogicIndexVerySlowlyWorkingDontUseMe();
	testGetLogicalIndexVerySlowlyWorkingDontUseMe();
	testLogic();
	testSortList();

	return 0;
}
